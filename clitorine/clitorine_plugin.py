from random import randint

import irc3
from irc3.plugins.command import command

from fetch_sqlite import url_retrieval
from fetch_sqlite import random_url
from fetch_sqlite import list_subreddit
from fetch_sqlite import retrieve_subreddit
from fetch_sqlite import stat


@irc3.plugin
class Plugin(object):

    def __init__(self, bot):
        self.bot = bot
        self.liste_message = [
                "Branle-toi bien le cul",
                "Profite-bien mon salaud",
                "J'espère que ça va te faire plaisir",
                "Tiens coquin"
            ]

    @irc3.event(irc3.rfc.JOIN)
    def say_hi(self, mask, channel, **kw):
        """Say something when someone join a channel"""
        if mask.nick != self.bot.nick:
            if mask.nick == "Steap":
                self.bot.privmsg(channel, "Oh non voilà l'autre pd...")
            else:
                self.bot.privmsg(channel, 'Bonjour %s!' % mask.nick)
        else:
            self.bot.privmsg(channel, 'Hey mofos!')

    @command(permission='view')
    def surpriseme(self, mask, channel, args):
        """surpriseme

            %%surpriseme
        """
        message = ""
        url = None

        url = random_url()

        message = (self.liste_message[randint(0, len(self.liste_message) - 1)])

        yield message + " : " + url[0] + " " + url[1]

    @command(permission='view')
    def list(self, mask, channel, args):
        """list

            %%list
        """
        liste_subreddit = list_subreddit()
        message = ""

        for subreddit in liste_subreddit:
            if subreddit == liste_subreddit[0]:
                message = subreddit
            else:
                message = message + " " + subreddit

        yield message

    @command(permission='view')
    def reverse(self, mask, channel, args):
        """reverse

            %%reverse <hash>
        """
        digest = args['<hash>']
        message = ""
        subreddit = ""

        subreddit = retrieve_subreddit(digest)

        message = digest + " -> " + subreddit

        yield message

    @command(permission='view')
    def gimme(self, mask, channel, args):
        """gimme

            %%gimme <subreddit>
        """
        subreddit = args['<subreddit>']
        message = ""
        url = None

        message = (self.liste_message[randint(0, len(self.liste_message) - 1)])

        url = url_retrieval(subreddit)
        yield message + " : " + url[0]

    @command(permission='view')
    def stat(self, mask, channel, args):
        """stat

            %%stat
        """
        unread = None
        read = None
        total = None

        unread, read, total = stat()

        yield "Il y a " + str(unread) + " url non vues, " + str(read) \
            + " url vues ce qui fait un total de " + str(total)

    @command(permission='view')
    def porn(self, mask, channel, args):
        """porn

            %%porn
        """
        message = ""
        url = None

        url, digest = random_url()
        subreddit = retrieve_subreddit(digest)

        message = (self.liste_message[randint(0, len(self.liste_message) - 1)])

        yield message + " : " + url + " (qui vient de " + subreddit + ")"
