#!/usr/bin/env python3

import base64
import hashlib
import sqlite3
import sys
import time
from urllib.parse import urlparse

import praw
import requests

DBFILE = './editoir.db'
OK_DOMAINS = ["imgur.com", "i.imgur.com", "gfycat.com", "i.reddituploads.com"]


def check_url(url):
    # if the url is a redirection, first thing: follow it
    url = requests.head(url, allow_redirects=True).url
    parsed_url = urlparse(url)
    if parsed_url.netloc in OK_DOMAINS:

        if parsed_url.scheme == "http":
            https_url = 'https' + url[4:]
            r = requests.head(https_url, timeout=2)
            if r.status_code == 200:
                return https_url

        elif parsed_url.scheme == "https":
            r = requests.head(url, timeout=2)
            if r.status_code == 200:
                return url

    return


def digest_string(url):
    if isinstance(url, str):
        digest = hashlib.sha1(url.encode('utf-8'))
        b64digest = base64.b64encode(digest.digest())
        return str(b64digest.decode('utf-8'))
    else:
        print("url is not a string: " + url)
        sys.exit(1)


def put_url_db(url, digest, subreddit):
    conn = sqlite3.connect(DBFILE)
    c = conn.cursor()

    c.execute(
        "INSERT INTO urls (url, digest, subreddit, given) VALUES (?,?,?,0)",
        (url, digest, subreddit))
    conn.commit()
    conn.close()


def fetch_url(subreddit):
    r = praw.Reddit(user_agent='clitorine-0.1')
    try:
        submissions = r.get_subreddit(subreddit).get_hot(limit=5)
        for submission in submissions:
            url = submission.url
            newurl = check_url(url)
            if newurl:
                fingerprint = digest_string(newurl)
                put_url_db(newurl, fingerprint, subreddit)
            else:
                print("NOPE'D " + url + " " + subreddit)
    except requests.exceptions.ConnectionError:
        print("ConnectionError while fetching " + subreddit)
        time.sleep(120)


def main():
    subreddits = ["cat_girls", "amateur", "collegeamateurs",
                  "PublicFlashing", "anal", "analporn", "asslick",
                  "GroupOfNudeGirls", "iWantToFuckHer", "TheUnderbun",
                  "CycleSeats", "ass", "O_Faces", "porn",
                  "doublepenetration", "onherknees", "shelikesitrough",
                  "nsfwhardcore", "NSFW_GIF", "onoff", "bonermaterial",
                  "unashamed", "Hotchickswithtattoos",
                  "HappyEmbarrassedGirls"]
    while True:
        for subreddit in subreddits:
            fetch_url(subreddit)
            time.sleep(120)
        time.sleep(10800)

if __name__ == '__main__':
    main()
