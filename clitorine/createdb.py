#!/usr/bin/env python3

import sqlite3
from os import remove

DBFILE = './editoir.db'


def initdatabase():
    try:
        remove(DBFILE)
    except OSError:
        pass

    conn = sqlite3.connect(DBFILE)
    c = conn.cursor()
    # Create the database
    print("database creation")
    c.execute("""CREATE TABLE
    urls (url TEXT, digest TEXT, subreddit TEXT, given INTEGER, UNIQUE (digest)
    ON CONFLICT IGNORE)""")
    conn.commit()
    conn.close()
    print("close")

if __name__ == "__main__":
    initdatabase()
