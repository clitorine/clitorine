#!/usr/bin/env python3

import random
import sqlite3
import sys

DBFILE = './editoir.db'


def url_retrieval(subreddit):
    conn = sqlite3.connect(DBFILE)
    c = conn.cursor()
    if subreddit not in list_subreddit():
        return "Wrong subreddit"

    c.execute("""SELECT url,digest FROM urls
                WHERE subreddit LIKE ? AND given = 0 LIMIT 1""",
              ('%' + subreddit + '%',))
    try:
        url, digest = c.fetchone()
    except TypeError:
        return ("No more link", "XXX")

    c.execute("UPDATE urls SET given=1 WHERE digest LIKE ?", (digest,))

    conn.commit()
    conn.close()

    return url, digest


def list_subreddit():
    conn = sqlite3.connect(DBFILE)
    c = conn.cursor()

    subreddit = []
    for row in c.execute("SELECT DISTINCT subreddit FROM urls"):
        subreddit.append(row[0])

    conn.commit()
    conn.close()

    return subreddit


def random_url():
    subreddit = (random.choice(list_subreddit()))
    url, digest = url_retrieval(subreddit)

    return url, digest


def retrieve_subreddit(digest):
    conn = sqlite3.connect(DBFILE)
    c = conn.cursor()

    c.execute(
        "SELECT url, subreddit FROM urls WHERE digest LIKE ?",
        ('%' + digest + '%',))
    try:
        url, subreddit = c.fetchone()
    except TypeError:
        return "No subreddit found for this digest"

    conn.commit()
    conn.close()

    return subreddit


def stat():
    conn = sqlite3.connect(DBFILE)
    c = conn.cursor()
    c.execute("select count(*) from urls where given like '0';")
    unread = c.fetchone()[0]

    c.execute("select count(*) from urls where given like '1';")
    read = c.fetchone()[0]

    conn.close()
    return unread, read, unread + read


def main():
    if len(sys.argv) > 1:
        if sys.argv[1] == "gimme":
            url, digest = url_retrieval(sys.argv[2])
            print(url)
        elif sys.argv[1] == "list":
            print(list_subreddit())
        elif sys.argv[1] == "lucky":
            print(random_url())
        elif sys.argv[1] == "reverse":
            print(sys.argv[2])
            print(retrieve_subreddit(sys.argv[2]))

if __name__ == '__main__':
    main()
